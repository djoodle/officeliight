# My Office Lights
A WFH with children hack to void one of these:

![WFH With Kids](https://media1.giphy.com/media/8HEDXbMiz8EcZEY024/giphy.gif?cid=ecf05e47o4222isg7yqhqihai4tg8l52lm3wv8hxfn3xs8fc&ep=v1_gifs_search&rid=giphy.gif&ct=g)

## Use
Run the script. I leave it running - can be run as a daemon - whatever you prefer:

`node light.js`

When you join a slack huddle or join a google meet the lights go red. When you leave they go back to green. Now the hard part is conditioning your children to understand what that _means_ - here I offer you no help.

![Script Usage](gifs/usage.gif)
![Lights](gifs/lights.gif)

If you send the process a SIGUSR1 signal it will also flash blue (blue + red/green depending on the onAir state). I call this _energenTea_ . Conditioning children/partnets/pets to understand and bring tea is done
at your own peril. I wrapped this into a shell function:

```
function tea
    kill -s SIGUSR1 $(cat ~/dev/officeliight/.pid)
end
```

(my shell is fishy - other shells exist)

## Hardward
I used a tapo L900 strip light. At the time it was about 7squid.

## Code
Wrote it in javascript as there was already a basic tapo library.

### Detecting Mac Camera Use
To detect camera use I query the process list like `ps -ef | grep "VideoCaptureService" || true`. Chrome starts this helper service whenver the camera is in use.

### Detecting Mic Use
Cause I also don't want y'all to hear the angels in the background "Daddy.... Daaaaaaddddyyyyy...." I also 
check to see if the mic is active. For this I i check the IO registry to see what state the IoAudioEngine is like so: `ioreg -l | grep \'"IOAudioEngineState" = 1\' || true`