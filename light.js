import { loginDeviceByIp, turnOn, setBrightness, turnOff, getDeviceInfo, setColour } from 'tp-link-tapo-connect';
import { promisify } from 'util';
import {setTimeout} from "timers/promises";
import { exec } from 'child_process';
import log from 'npmlog';
import Arpping from 'arpping';
import cidrRange from 'cidr-range';
import * as dotenv from 'dotenv';
import * as path from 'path';
import { fileURLToPath } from 'url';
import {createWriteStream} from 'fs';
const pexec = promisify(exec);
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
dotenv.config();

let inMeeting = false;
let lock = false;

const startUp = async (mac, user, pass) => {
  let ip = process.env.LIGHT_IP
  if(!ip){
    const arpping = new Arpping({timeout: 5});
    const [gateway] = cidrRange(process.env.LIGHT_GATEWAY || arpping.myDevice.connection.cidr).slice(-1);
    // refresh mac cache
    log.info('startup', 'refresh mac cache');
    await arpping.ping([gateway]);
    log.info('startup', 'finding device with mac', mac);
    const { hosts } = await arpping.searchByMacAddress([mac]);
    ip = hosts[0].ip;
    log.info('startip', 'found device with IP', ip);
  }
  log.info('startup', 'logging in to device', user, ip);
  const tkn = await loginDeviceByIp(user, pass, ip);
  await turnOn(tkn);
  log.info('startup', 'device turned on')
  modifyDeviceColour('green', tkn);
  process.on('SIGUSR1', () => {
    log.info('main', 'caught signal')
    emergenTea(tkn);
  });
  return tkn;
}

const isCamOn = async () => {
  const { stdout } = await pexec('ps -ef | grep "VideoCaptureService" || true');
  const status = stdout.toString().split('\n').length > 3;
  log.info('isCamOn', 'cam on status is', {status});
  return status;
}

const isMicOn = async () => {
  const { stdout } = await pexec('ioreg -l | grep \'"IOAudioEngineState" = 1\' || true');
  const status = stdout.toString().split('\n').length > 1;
  log.info('isMicOn', 'mic status is', {status});
  return status;
}
  
const modifyDeviceColour = async (colour, tkn) => {
  await setColour(tkn, colour);
  await setBrightness(tkn, 15);
  log.info('modifyDeviceColour', 'device colour changed', {colour});
}

const flash = async (tkn, colour) => {
  let iter = 0;
  log.info('flash', 'flashing the lights', {colour});
  const int = await setInterval(async () => {
    iter = iter + 1;
    await modifyDeviceColour(colour, tkn);
    await setTimeout(500);
    await modifyDeviceColour((inMeeting ? 'red' : 'green'), tkn);
    if(iter >= 10) {
      clearInterval(int);
      return;
    }
  }, 1000);
  await modifyDeviceColour((inMeeting ? 'red' : 'green'), tkn);
}

const emergenTea = async (tkn) => {
  log.info('emergenTea', 'triggered tea request');
  lock = true;
  await flash(tkn, 'blue');
  log.info('emergenTea', 'tea request timed out');
  lock = false;
}

const onAir = async (tkn) => {
  const live = await isCamOn() || await isMicOn() || false; 
  log.info('onAir', 'returned status', {live, inMeeting});
  if(live && !inMeeting && !lock) {
    inMeeting = true;
    await modifyDeviceColour('red', tkn);
  } else if(!live && inMeeting && !lock) {
    inMeeting = false;
    await modifyDeviceColour('green', tkn);
  }
}

async function repeat(tkn) {
  if (!lock) {
    await onAir(tkn);
  }
  const loopInterval = inMeeting ? 30000 : 10000;
  await setTimeout(loopInterval);
  repeat(tkn);
};

log.info('main', 'starting up', {pid: process.pid});

const pidFile = createWriteStream(__dirname + '/.pid',{flags: 'w', encoding: 'utf-8'});
pidFile.write(process.pid.toString());
pidFile.end();

await repeat(await startUp(
  process.env.LIGHT_MAC_ADDR, 
  process.env.LIGHT_USER,
  process.env.LIGHT_PASSWD,
));